package de.avono.samples.jersey_request_logging;

import org.glassfish.jersey.filter.LoggingFilter;

import javax.annotation.Priority;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;

@Priority(Integer.MAX_VALUE)
public class PrioritizedLoggingFilter implements ContainerRequestFilter, ContainerResponseFilter, WriterInterceptor
{
   private final LoggingFilter delegate = new LoggingFilter();

   public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException
   {
      delegate.filter(requestContext, responseContext);
   }

   public void filter(ContainerRequestContext context) throws IOException
   {
      delegate.filter(context);
   }

   public void aroundWriteTo(WriterInterceptorContext writerInterceptorContext) throws IOException, WebApplicationException
   {
      delegate.aroundWriteTo(writerInterceptorContext);
   }
}
