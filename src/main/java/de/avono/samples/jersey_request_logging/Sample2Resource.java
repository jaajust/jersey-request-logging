package de.avono.samples.jersey_request_logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/sample2")
public class Sample2Resource
{
   private final static Logger LOGGER = LoggerFactory.getLogger(Sample2Resource.class);

   @GET
   public String get()
   {
      LOGGER.info("Sample2Resource.get.");
      return "Sample2";
   }
}
