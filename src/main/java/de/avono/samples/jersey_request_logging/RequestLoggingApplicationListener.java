package de.avono.samples.jersey_request_logging;

import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestLoggingApplicationListener implements ApplicationEventListener
{
   public void onEvent(ApplicationEvent applicationEvent) {
   }

   public RequestEventListener onRequest(RequestEvent requestEvent)
   {
      return LoggingRequestEventListener.INSTANCE;
   }

   private static class LoggingRequestEventListener implements RequestEventListener
   {
      public static final LoggingRequestEventListener INSTANCE = new LoggingRequestEventListener();
      private static final Logger LOGGER = LoggerFactory.getLogger(LoggingRequestEventListener.class);

      public void onEvent(RequestEvent requestEvent)
      {
         final ContainerRequest containerRequest = requestEvent.getContainerRequest();
         switch (requestEvent.getType())
         {
            case REQUEST_FILTERED:
               // first event that is called, after all request filters are called
               LOGGER.info("Start request {} {}", containerRequest.getMethod(), containerRequest.getRequestUri());
               break;
            case RESP_FILTERS_START:
               // first event that contains a response
               LOGGER.info("End request {} {} {}", containerRequest.getMethod(), containerRequest.getRequestUri(), requestEvent.getContainerResponse().getStatus());
               break;
         }
      }
   }
}
