package de.avono.samples.jersey_request_logging;

import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/jersey-request-logging")
public class RequestLoggingApplication extends Application
{
   @Override
   public Set<Class<?>> getClasses()
   {
      SLF4JBridgeHandler.removeHandlersForRootLogger();
      SLF4JBridgeHandler.install();
      final Set<Class<?>> classes = new HashSet<Class<?>>();
      classes.add(Sample1Resource.class);
      classes.add(Sample2Resource.class);
      return classes;
   }

   @Override
   public Set<Object> getSingletons()
   {
      final Set<Object> singletons = new HashSet<Object>();
      //singletons.add(new org.glassfish.jersey.filter.LoggingFilter());
      //singletons.add(new PrioritizedLoggingFilter());
      singletons.add(new MDCFillingFilter());
      singletons.add(new RequestLoggingApplicationListener());
      return singletons;
   }
}
