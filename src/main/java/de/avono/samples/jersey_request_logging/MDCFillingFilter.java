package de.avono.samples.jersey_request_logging;

import org.slf4j.MDC;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;

public class MDCFillingFilter implements ContainerRequestFilter, ContainerResponseFilter
{

   public void filter(ContainerRequestContext requestContext) throws IOException
   {
      MDC.put("id", "MDC-Value");
   }

   public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException
   {
      MDC.remove("id");
   }
}
