package de.avono.samples.jersey_request_logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/sample1")
public class Sample1Resource
{
   private final static Logger LOGGER = LoggerFactory.getLogger(Sample1Resource.class);

   @GET
   public String get()
   {
      LOGGER.info("Sample1Resource.get.");
      return "Sample1";
   }
}
