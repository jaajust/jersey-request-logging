# README #
Dieses Projekt veranschaulicht die Thematik von [Request-Logging mit JAX-RS und Jersey](https://www.avono.de/blog/request-logging-mit-jax-rs-und-jersey)

Um die Beispiel-Webanwendung zu starten `mvn jetty:run` ausführen.

Um zu sehen, was die Anwendung in die Logs schreibt, muss man eine URL der Anwendung aufrufen. Z.B. so:
`curl http://localhost:8080/jersey-request-logging/sample1`

## Alternativlösungen ##
### Jersey-LoggingFilter ###
Um zu sehen wie sich die Anwendung verhält, wenn man den LoggingFilter von Jersey verwendet:

*    `git checkout LoggingFilter`
*    Die Schritte oben ausführen

### Priorisierter Jersey-LoggingFilter ###
Um zu sehen wie sich die Anwendung verhält, wenn man den LoggingFilter von Jersey anderes priotisiert:

*    `git checkout PrioritizedLoggingFilter`
*    Die Schritte oben ausführen